# Predlog projekta

| | |
|:---|:---|
| **Naziv projekta** | ŠtudentskiLajf |
| **Člani projektne skupine** | Nik Bratuša, Sara Koljić, Žan Pečovnik, Žiga Kleine |
| **Kraj in datum** | 18. 3. 2019, Ljubljana |



## Povzetek projekta
Glavni cilj projekta je vzpostavitev spletne aplikacije, ki ponuja uporabniku
enostavno uporabo vseh svojih najljubših in najpomembnejših funkcionalnosti na enem mestu. 
Ker je naša aplikacija namenjena skoraj izključno študentom in dijakom bodo tudi funkcionalnosti
prilagojene le temu. To pomeni vsebovanje urnika, redovalnice, 
hitre povezave do najljubših voznih redov LPP-ja ter jedilniki
in lokacije najljubših restavracij. 
Za to bomo uporabili spletne tehnologije na front-endu in back-endu, ki bodo omogočale
uporabniku registracijo in uporabo aplikacije po svoji želji. Projekt bo razdeljen
na 4 glavne točke, ki se bodo izvajale po korakih, delo pa bo razdeljeno enakovredno
med člane ekipe glede na njihove želje in sposobnosti. Za uspešno sodelovanje 
bomo uporabljali online komunikacijo kot tudi redne sestanke v živo.


## 1. Motivacija
Študentje se vsakodnevno srečujemo z določenimi problematikami skozi dan, te ponavadi obsegajo
pregled nad dejavnostmi, ki jih imamo čez dan, prevoz na fax in domov in nato še seveda hrana.
Ker kot sami študentje vemo, kako naporno zna biti iskanje vseh
naštetih informacij po morda treh različnih mestih, se nam zdi zelo smiselno oz. celo nujno
potrebno ustvariti aplikacijo, ki bi zadovoljila vse naše potrebe na enem mestu. Prav v tem
pa se naša aplikacija tudi razlikuje od drugih, ker pravzaprav takšne še ni.
Zajema torej enostaven in prilagojen dostop do tem, 
ki so uporabne za vsakega študenta. Dodatna prednost je še možnost
visoke mere personalizacije, kar pomeni da si vsak študent uredi stran po svojih željah in potrebah.
Jasno je, da je naša programska rešitev namenjena
uporabi študentom v Sloveniji s povdarkom tistih v Ljubljani.

## 2. Cilji projekta in pričakovani rezultati

### 2.1 Opis ciljev

Cilj naše aplikacije je združiti velike količine informacij, ki so v današnjem času dostopne in koristne študentom, na eno mesto. 
Aplikacija bi podatke združila na pregleden način, ki bi omogočal visok nivo personalizacije.
S tem bi uporabniku aplikacije prihranili čas, saj mu ne bi bilo treba dostopati do večih spletnih portalov. 
Vsak uporabnik bi si lahko urejal podatke o mestnem prometu, študentski prehrani, urnikih in ocenah na način, ki bi mu najbolje ustrezal.

### 2.2 Pričakovani rezultati


Pričakovani rezultat projekta je spletna aplikacija, ki bi delovala na mobilnih telefonih in računalnikih s kompatibilnimi spletnimi brskalniki.
Vsebovala bo registracijo in prijavo v sistem z uporabnikovimi osebnimi podatki. Ko bi se uporabnik prijavil v aplikacijo, bi imel na voljo urnik,
v katerega bi vpisal svoj urnik iz fakultete, lahko pa bi dodal še svoje ostale aktivnosti v tekočem tednu. V povezavi z urnikom bo na voljo še redovalnica,
kamor bo lahko uporabnik vnesel svoje ocene. Aplikacija bo vsebovala še integracijo z lpp ter portalom študentske prehrane.
Uporabnik si bo lahko shranil najpogosteje uporabljene restavracije in linije mestnega prometa, kar mu bo omogočalo hiter dostop do podatkov o le-teh. 
Uporabili bomo html, css in javaScript s pomočjo MEAN arhitekture.

## 3. Projektni načrt


### 3.1 Povzetek razdelitve projekta na aktivnosti

Projekt je sestavljen iz 3 faz in vsaka faza vsebuje več aktivnosti.

Prva faza vsebuje delo na zajemu zahtev, obvladovanju projekta in tveganj
ter izobraževanje o problemski domeni.

V drugi fazi je vključeno kodiranje, testiranje in debugiranje front-end logike, kodiranje, testiranje in debugiranje
back-end logike, kodiranje, testiranje in debugiranje povezave s podatkovno bazo ter integracijo, testiranje in
debugiranje vseh teh treh nivojev kot celote.

V zadnji, zaključni fazi je potrebno dokončati dokumentacijo in pripraviti predstavitev produkta.


### 3.2 Načrt posameznih aktivnosti

1 ČM = 60 ur  
0,05 ČM = 1 dan(3 ure)

| **Oznaka aktivnosti** | A1 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 25. 3. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 29. 3. 2019 |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Zajem zahtev |
| **Obseg aktivnosti v ČM** | 0,25 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Izgled in zgradba končne spletne strani, podroben načrt projekta in osnovnih zahtev |
| **Opis aktivnosti** | Člani delovne skupine določijo vse podrobnosti končnega projekta, nato pa še arhitekturo aplikacije in zasnujejo osnovo te arhitekture, torej določijo kakšne tehnologije, razvojne platforme in razvojna orodja bodo uporabili. |
| **Morebitne odvisnosti in omejitve** | To je prva aktivnost in ni odvisna od drugih aktivnosti, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Osnutek projekta in specifikacija zahtev |


| **Oznaka aktivnosti** | A2 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 1. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 2. 4. 2019 |
| **Trajanje** | 2 dneva  |
| **Naziv aktivnosti** | Obvladovanje projekta in tveganj |
| **Obseg aktivnosti v ČM** | 0,1 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Narediti plan obvladovanja projekta in plan obvladovanja tveganj |
| **Opis aktivnosti** | Odločili se bomo kako bomo učinkovito upravljali zadani projekt. Odločili se bomo kako bo potekala komunikacija v ekipi, kako si bomo delo razdelili in med seboj sodelovali. Ugotovili bomo tudi katera tveganja bodo pri projektu prisotna in ta tveganja ocenili.|
| **Morebitne odvisnosti in omejitve** | A2 ni odvisna od nobene aktivnosti. |
| **Pričakovani rezultati aktivnosti** | Predlog projekta in plan obladovanja le-tega,načini učinkovitega dela na projektu, seznam in ocene tveganj. |


| **Oznaka aktivnosti** | A3 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 3. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 5. 4. 2019 |
| **Trajanje** | 3 dnevi |
| **Naziv aktivnosti** | Izobraževanje o problemski domeni |
| **Obseg aktivnosti v ČM** | 0,15 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Vsak razvijalec se spozna z tehnologijami, ki so potrebne za nadaljnji uspešni razvoj aplikacije. |
| **Opis aktivnosti** | Člani ekipe se bodo sami usposabljali na področju izdelave interaktivnih aplikacij. Poskusili bodo čim bolje razumeti dano nalogo in načinov za izvršitve le te.|
| **Morebitne odvisnosti in omejitve** | A3 ni odvisna od nobene aktivnosti. |
| **Pričakovani rezultati aktivnosti** | Izboljšano in nadgrajeno znanje članov ekipe na področju razvoja spletnih aplikacij |


| **Oznaka aktivnosti** | A4 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 8. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 9. 4. 2019 |
| **Trajanje** | 2 dneva |
| **Naziv aktivnosti** | Načrtovanje arhitekture |
| **Obseg aktivnosti v ČM** | 0,1 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Natančno opredeljena zgradba spletne aplikacije oz. njene arhitekture |
| **Opis aktivnosti** | Člani delovne skupine določijo kako bo zgrajena arhitektura spletne aplikacije. Arhitekturo je treba zasnovati tako, da bo back-end deloval brez težav oz. da ne bo preobremenjena povezava med bazo in back-end delom. |
| **Morebitne odvisnosti in omejitve** | A4 je odvisna od aktivnosti A1 ter A3, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt arhitekture aplikacije |


| **Oznaka aktivnosti** | A5 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 10. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 16. 4. 2019 |
| **Trajanje** | 7 dni |
| **Naziv aktivnosti** | Načrtovanje izgleda in delovanja urnika, redovalnice ter strani za vpis |
| **Obseg aktivnosti v ČM** | 0,05 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Natančno opredeljen izgled in delovanje strani z urnikom, strani z redovalnico in strani, kjer bo možen vpis v aplikacijo. |
| **Opis aktivnosti** | Člani delovne skupine natančno določijo katere tehnologije bodo uporabili (JavaScript, CSS, HTML, ...), kako bo izgledala spletna stran, ki bo vsebovala urnik, kako bo izgledala spletna stran z redovalnico ter kako bo izgledala spletna stran, kjer se bo lahko posameznik vpisal v spletno aplikacijo. |
| **Morebitne odvisnosti in omejitve** | A5 je odvisna od aktivnosti A4, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt delovanja in izgleda urnika, redovalnice ter strani za vpis |


| **Oznaka aktivnosti** | A6 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 10. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 16. 4. 2019 |
| **Trajanje** | 7 dni |
| **Naziv aktivnosti** | Načrtovanje izgleda in delovanja urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |
| **Obseg aktivnosti v ČM** | 0,1 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Natančno opredeljen izgled in delovanje urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |
| **Opis aktivnosti** | Člani delovne skupine natančno določijo katere tehnologije bodo uporabili (JavaScript, CSS, HTML, ...), kako bo izgledala spletna stran, kjer si bo posameznik lahko izbral linijo in avtobusno postajo mestnega prometa v Ljubljani, nato pa se mu bo prikazal ustrezen urnik avtobusnega prevoza. Člani nato še določijo izgled spletne strani, kjer bo posameznik lahko poiskal vse restavracije, ki ponujajo prehrano na študentski bon, ob izbiri posamezne restavracije pa se mu bo prikazal še jedilnik za tisti dan. |
| **Morebitne odvisnosti in omejitve** | A6 je odvisna od aktivnosti A4, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt delovanja in izgleda urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |


| **Oznaka aktivnosti** | A7 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 17. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 18. 4. 2019 |
| **Trajanje** | 2 dneva |
| **Naziv aktivnosti** | Načrtovanje podatkovne baze |
| **Obseg aktivnosti v ČM** | 0,1 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Izdelan načrt podatkovne baze |
| **Opis aktivnosti** | ČLani delovne skupine določijo katero podatkovno bazo bodo uporabljali (relacijsko ali nerelacijsko), določijo odvisnosti med podatki v podatkovni bazi in sheme za podatke. |
| **Morebitne odvisnosti in omejitve** | A7 je odvisna od aktivnosti A4, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt podatkovne baze |


| **Oznaka aktivnosti** | A8 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 19. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 21. 4. 2019 |
| **Trajanje** | 3 dnevi |
| **Naziv aktivnosti** | Načrtovanje testiranja |
| **Obseg aktivnosti v ČM** | 0,05 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Izdelan načrt testiranja front-end in back-end dela strani z urnikom, redovalnico, vpisom, urnikom avtobusov, lokacijami in jedilniki študentske prehrane |
| **Opis aktivnosti** | Člani ekipe določijo kakšne teste bodo opravili po končani implementaciji in v kakšnem obsegu bodo opravili te teste. |
| **Morebitne odvisnosti in omejitve** | A8 je odvisna od aktivnosti A5, A6 in A7, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt testiranja |


| **Oznaka aktivnosti** | A9 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 26. 5. 2019 |
| **Trajanje** | 35 dni |
| **Naziv aktivnosti** | Project management |
| **Obseg aktivnosti v ČM** | 0,05 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Izdelan načrt obvladovanja projekta |
| **Opis aktivnosti** | Člani ekipe določijo kako bo potekala medsebojna komunikacija, sodelovanje in delitev dela. Vsak dan bo kratek sestanek, kjer se bodo dogovorili, kaj je potrebno še narediti in pregledali kaj so že naredili. |
| **Morebitne odvisnosti in omejitve** | A9 ni odvisna od nobene aktivnosti. |
| **Pričakovani rezultati aktivnosti** | Načrt obvladovanja projekta |


| **Oznaka aktivnosti** | A10 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 26. 4. 2019 |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Razvijanje izgleda in delovanja urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |
| **Obseg aktivnosti v ČM** | 0,09 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Končen izgled in pravilno delovanje urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |
| **Opis aktivnosti** | Člani ekipe z izbranimi tehnologijami razvijejo spletne strani, tako da zadovoljijo izdelanemu načrtu (A6), kar vključuje razporeditev elementov na zaslonu, uporabljene barve in font pisave, vpeljava animacij ter ustrezen prikaz izbranih podatkov. |
| **Morebitne odvisnosti in omejitve** | A10 je odvisna od aktivnosti A6, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Končen izgled in pravilno delovanje urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |


| **Oznaka aktivnosti** | A11 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 26. 4. 2019 |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Razvijanje izgleda in delovanja strani z urnikom, strani z redovalnico in strani, kjer bo možen vpis v aplikacijo. |
| **Obseg aktivnosti v ČM** | 0,12 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Končen izgled in pravilno delovanje strani z urnikom, strani z redovalnico in strani, kjer bo možen vpis v aplikacijo. |
| **Opis aktivnosti** | Člani ekipe z izbranimi tehnologijami razvijejo spletne strani, tako da zadovoljijo izdelanemu načrtu (A5), kar vključuje razporeditev elementov na zaslonu, uporabljene barve in font pisave, vpeljava animacij ter ustrezen prikaz izbranih podatkov. |
| **Morebitne odvisnosti in omejitve** | A11 je odvisna od aktivnosti A5, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Končen izgled in pravilno delovanje strani z urnikom, strani z redovalnico in strani, kjer bo možen vpis v aplikacijo. |


| **Oznaka aktivnosti** | A12 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 27. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 29. 4. 2019 |
| **Trajanje** | 3 dnevi |
| **Naziv aktivnosti** | Testiranje front-end |
| **Obseg aktivnosti v ČM** | 0,05 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Zaznavanje morebitnih napak na front-end in odpravljanje le-teh |
| **Opis aktivnosti** | Člani testirajo možne klike in vnosna polja, najprej z bolj normalnimi primeri in če ti primeri uspešno prestanejo teste, je potrebno testirati še z bolj robnimi primeri. Vse te teste je potrebno narediti na različnih operacijskih sistemih in v različnih spletnih brskalnikih. Če najdemo kakšno napako jo je potrebno odpraviti in potem celoten postopek še enkrat ponoviti. |
| **Morebitne odvisnosti in omejitve** | A12 je odvisna od aktivnosti A10 in A11, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Popravljene morebitne napake na izgledu strani |


| **Oznaka aktivnosti** | A13 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 30. 4. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 3. 5. 2019 |
| **Trajanje** | 4 dnevi |
| **Naziv aktivnosti** | Razvijanje podatkovne baze |
| **Obseg aktivnosti v ČM** | 0,07 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Razvita podatkovna baza z vsemi odvisnostmi |
| **Opis aktivnosti** | ČLani razvijejo podatkovno bazo, da zadovoljijo potrebam načrta (A7), torej določijo v kakšni shemi bodo podatki, kakšne tabele bo podatkovna baza vsebovala, določijo odvisnosti med tabelami oziroma med shemami. |
| **Morebitne odvisnosti in omejitve** | A13 je odvisna od aktivnosti A7, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Delujoča podatkovna baza |


| **Oznaka aktivnosti** | A14 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 4. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 6. 5. 2019 |
| **Trajanje** | 3 dnevi |
| **Naziv aktivnosti** | Testiranje podatkovne baze |
| **Obseg aktivnosti v ČM** | 0,03 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Zaznavanje morebitnih napak pri shranjevanju podatkov v podatkovno bazo oziroma pri pridobivanju podatkov s pomočjo API-jev. |
| **Opis aktivnosti** | Člani skupine shranijo nekaj testnih podatkov v podatkovno bazo, da vidijo ali se uspešno shranijo, nato pa jih poskušajo s poizvedbami še pridobiti. Če odkrijejo kakšno napako jo popravijo in nato celoten postopek ponovijo od začetka. |
| **Morebitne odvisnosti in omejitve** | A14 je odvisna od aktivnosti A13, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Popravljene morebitne napake pri shranjevanju podatkov v podatkovno bazo oziroma pri pridobivanju podatkov s pomočjo API-jev |


| **Oznaka aktivnosti** | A15 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 7. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 13. 5. 2019 |
| **Trajanje** | 7 dni |
| **Naziv aktivnosti** | Razvijanje back-end urnika, redovalnice in vpisa v aplikacijo. |
| **Obseg aktivnosti v ČM** | 0,09 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Pridobivanje podatkov iz podatkovne baze za prikaz na front-end. Preverjanje podatkov za vpis. |
| **Opis aktivnosti** | Člani ekipe naredijo REST API, preko katerega bo možno shranjevanje podatkov urnika in redovalnice v podatkovno bazo in pridobivanje podatkov za prikaz na front-end za vsakega posameznika posebej. Ko se uporabnik vpiše v aplikacijo, je potrebno preveriti pravilnost podatkov v podatkovni bazi in odobriti dostop do osebnih podatkov in aplikacije ali pa uporabnika preusmeriti na stran za registracijo. |
| **Morebitne odvisnosti in omejitve** | A15 je odvisna od aktivnosti A11 in A13, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Pravilno pridobivanje podatkov iz podatkovne baze in pravilno preverjanje podatkov za vpis |


| **Oznaka aktivnosti** | A16 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 7. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 13. 5. 2019 |
| **Trajanje** | 7 dni |
| **Naziv aktivnosti** | Ravzijanje back-end urnika avtobusov, strani z lokacijami in jedilniki študentske prehrane |
| **Obseg aktivnosti v ČM** | 0,17 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Pridobivanje podatkov s pomočjo API-jev za prikaz na front-end. |
| **Opis aktivnosti** | Člani ekipe s pomočjo zunanjega API-ja pridobijo podatke o avtobusnih linijah in postajališčih in s pomočjo drugega zunanjega API-ja pridobijo podatke o restavracijah v Ljubljani, kjer ponujajo prehrano na študentske bone, ob izbiri določene restavracije pa se prikaže jedilnik za tisti dan. |
| **Morebitne odvisnosti in omejitve** | A16 je odvisna od aktivnosti A12 in A13, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Pravilno pridobljeni in pravilno prikazani podatki, ki so bili pridobljeni s pomočjo API-jev |


| **Oznaka aktivnosti** | A17 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 14. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 14. 5. 2019 |
| **Trajanje** | 1 dan |
| **Naziv aktivnosti** | Testiranje back-end |
| **Obseg aktivnosti v ČM** | 0,03 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Zaznavanje morebitnih napak pri pridobivanju podatkov iz podatkovne baze oziroma s pomočjo API-jev. |
| **Opis aktivnosti** | Člani ekipe naredijo teste, s katerimi pridobijo podatke s pomočjo REST API-ja oziroma s pomočjo zunanjih API-jev, nato poskušajo nekatere podatke preko REST API-ja še dodati v podatkovno bazo, če kjerkoli opazijo, da je pri shranjevanju ali pridobivanju podatkov prišlo do napake, le-to popravijo in postopek od začetka ponovijo. |
| **Morebitne odvisnosti in omejitve** | A17 je odvisna od aktivnosti A15 in A16, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Popravljene morebitne napake pri pridobivanju podatkov iz podatkovne baze oziroma s pomočjo API-jev |


| **Oznaka aktivnosti** | A18 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 15. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 17. 5. 2019 |
| **Trajanje** | 3 dnevi |
| **Naziv aktivnosti** | Integracija front-end, back-end in podatkovne baze v celoto |
| **Obseg aktivnosti v ČM** | 0,14 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Združitev vseh treh nivojev (front-end, back-end in podatkovna baza) v celoto. |
| **Opis aktivnosti** | Člani integrirajo back-end, front-end in podatkovno bazo v povezano celoto, tako da se posamezni deli med front-endom in back-endom povezujejo ter dopolnjujejo med sabo, tako da je aplikacija v celoti delujoča in pripravljena za širšo uporabo. |
| **Morebitne odvisnosti in omejitve** | A18 je odvisna od aktivnosti A14, A17, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Uspešna združitev vseh treh nivojev v celoto |


| **Oznaka aktivnosti** | A19 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 18. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 21. 5. 2019 |
| **Trajanje** | 4 dnevi |
| **Naziv aktivnosti** | Testiranje integrirane celote |
| **Obseg aktivnosti v ČM** | 0,07 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Zaznavanje morebitnih napak, ki so nastale pri združevanju v celoto. |
| **Opis aktivnosti** | Člani opravijo teste z vnašanjem podatkov v vnosna polja in klikanjem gumbov, tako kot bodo potem uporabniki uporabljali aplikacijo, da vidijo, če se vsi podatki pravilno prikažejo, če se pravilno pridobivajo in shranjujejo v podatkovno bazo, če deluje vpis in registracija v spletno aplikacijo. Če opazijo kakšno napako, jo popravijo in potem postopek testiranja v celoti od začetka ponovijo. |
| **Morebitne odvisnosti in omejitve** | A19 je odvisna od aktivnosti A18, je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Popravljene morebitne napake, ki so nastale pri združevanju v celoto |


| **Oznaka aktivnosti** | A20 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 23. 5. 2019 |
| **Trajanje** | 2 dneva |
| **Naziv aktivnosti** | Dokumentacija |
| **Obseg aktivnosti v ČM** | 0,07 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Združitev vse dokumentacije, ki je nastala med projektom in pisanje manjkajoče dokumentacije. |
| **Opis aktivnosti** | Dokumentacija je najbolj pomemben del skupinskega projekta, člani že sproti vsako aktivnost natančno dokumentirajo, da lahko potem ljudje, ki se niso ukvarjali s tem projektom dobijo jasno sliko, kako je bila aplikacija ustvarjena, lažje je tudi kasneje odpravljati napake, saj lahko v dokumentaciji hitro poiščemo, kje bi se takšna napaka lahko pripetila. V zadnjem koraku vso dokumentacijo, ki je nastajala čez celoten projekt člani združijo v celoto in dopolnijo še morebitne manjkajoče dele.  |
| **Morebitne odvisnosti in omejitve** | A20 ni odvisna od nobene aktivnosti. |
| **Pričakovani rezultati aktivnosti** | Nazorna dokumentacija, iz katere so razvidne vse podrobnosti dela na projektu |


| **Oznaka aktivnosti** | A21 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 24. 5. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 26. 5. 2019 |
| **Trajanje** | 3 dnevi |
| **Naziv aktivnosti** | Predstavitev |
| **Obseg aktivnosti v ČM** | 0,03 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Priprava zanimive in zabavne predstavitve projekta, ki bo razložila zakaj je ta projekt dober. |
| **Opis aktivnosti** | Člani pripravijo predstavitev projekta, ki bo zanimiva in zabavna ter bo na takšen način prepričala morebitne kupce, da bi aplikacijo kupili ali pa bi prepričala ljudi, da sploh postanejo uporabniki takšne aplikacije. Potrebno je izpostaviti prednosti projekta, v čem se aplikacija razlikuje od ostalih aplikacij, zaradi česa jo je vredno uporabljati. |
| **Morebitne odvisnosti in omejitve** | A21 ni odvisna od nobene aktivnosti. |
| **Pričakovani rezultati aktivnosti** | Predstavitev celotnega projekta, ki bo privlačila morebitne kupce ali uporabnike |

### 3.3 Seznam izdelkov

| Oznaka izdelka | Ime izdelka | Datum izdaje | 
| :--- | :---------------------------- | :--- |
| PI1 | Zajem zahtev | 29.3.2019 |
| PI2 | Načrt aplikacije | 21.4.2019 |
| PI3 | Zaslonske maske | 26.4.2019 |
| PI4 | Delujoč front-end | 29.4.2019 |
| PI5 | Delujoč back-end | 14.52019 |
| PI6 | Delujoča aplikacija | 21.5.2019 |
| PI7 | Dokumentacija | 23.5.2019 |
| PI8 | Zaključna predstavitev | 26.5.2019 |

### 3.4 Časovni potek projekta - Ganttov diagram

![Ganttov diagram](../img/ganttov_diagram.png)


### 3.5 Odvisnosti med aktivnosti - Graf PERT

![Graf PERT1](../img/graf-pert1.png)

![Graf PERT2](../img/graf-pert2.png)



## 4. Obvladovanje tveganj

### 4.1 Identifikacija in analiza tveganj

| **Naziv tveganja** | **Opis tveganja** | **Tip tveganja** | **Verjetnost nastopa tveganja** | **Posledice nastopa tveganja** |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
| Podcenjen čas razvoja | Čas razvoja programske opreme je podcenjen. | Ocenjevanje | Zmerna | Resne |
| Podcenjen obseg programa | Obseg programske opreme je podcenjen. | Ocenjevanje | Nizka | Dopustne |
| Slaba organizacija projekta | Zaradi slabe organizacije projekt časovno zaostaja. | Organizacijsko | Nizka | Resne |
| Neenakomerna obremenitev članov | Nekateri člani ekipe so bolj obremenjeni kot drugi. | Organizacijsko | Zmerna | Dopustne |
| Pomanjkanje znanja članov | Ekipa nima dovolj znanja za uspešen in dovolj hiter razvoj projekta. | Ljudje | Zmerna | Resne |
| Pomanjkanje časa članov | Člani ekipe nimajo dovolj časa, ki bi ga lahko namenili izdelavi projekta. | Ljudje | Nizka | Resne |
| Projekt ni konkurenčen | Predlog projekta ne zadošča zahtevam stranke, ni konkurenčen. | Zahteve | Visoka | Usodne |
| Zahteve niso časovno izvedljive | Zahteve strank niso časovno izvedljive za ekipo. | Zahteve | Nizka | Resne |
| Napačni podatki | Podatki, ki jih dobimo preko API-jev niso dovolj točni ali napačni. | Tehnologije | Zmerna | Dopustne |
| Počasno delovanje baze | Podatkovna baza, ki smo jo uporabili, deluje prepočasi. | Tehnologije | Nizka | Resne |
| Počasen strežnik | Delovanje strežnika, na katerem je spletna aplikacija gostovana, je prepočasno. | Orodja | Visoka | Dopustne |
| Okvara računalnika člana | Računalnik člana skupine se pokvari, zaradi tega ne more nadaljevati z delom. | Orodja | Zelo Nizka | Neznatne |

### 4.2 Načrtovanje tveganj

| Tveganje  | Strategija |
| :-------- | :--------- |
| Podcenjen čas razvoja | Člani namenijo dodaten čas razvoju projekta, manj pomembne funkcionalnosti izključimo iz projekta. |
| Podcenjen obseg programa | Člani namenijo dodaten čas razvoju projekta, manj pomembne funkcionalnosti izključimo iz projekta. |
| Slaba organizacija projekta | Izboljšamo načrt dela. |
| Neenakomerna obremenitev članov | Ponovno, enakomerneje razporedimo delo med člane ekipe glede a njihov čas in znanje. |
| Pomanjkanje znanja članov | Člani ekipe vložijo dodaten čas v izobraževanje o uporabljenih orodjih in tehnologijah, uporabimo orodja in tehnologije s katerimi je ekipa bole seznanjena. |
| Pomanjkanje časa članov | Delo reorganiziramo tako,  da ga imajo več člani ekipe z več časa. |
| Projekt ni konkurenčen | Projekt poskušamo narediti privlačnejši tako, da dodamo/izboljšamo funkcionalnosti, ki jih naša aplikacija ponuja. |
| Zahteve niso časovno izvedljive | Ekipa prosi stranko za več časa pri razvoju projekta. |
| Napačni podatki | Znotraj aplikacije opozorimo uporabnika, da podatki pridobljeni iz drugih spletnih portalov niso nujno natančni. |
| Počasno delovanje baze | Uporaba učinkovitejše in bolje načrtovane podatkovne baze. |
| Počasen strežnik | Namesto uporabe brezplačnega strežnika za gostovanje aplikacije, se odločimo za plačljivega. |
| Okvara računalnika | Član ekipe z okvarjenim računalnikom si poišče nadomestno napravo, ali pa uporablja računalnike na fakulteti. |



## 5. Upravljanje projekta

* Projekta se bomo lotili s skupinimi močmi in si delo pravično razdelili. Naloge, ki si jih bomo med sabo razdelili, bomo poskusili opraviti čimboljše in predvsem do zadanih rokov.
*Usklajevali se bomo z rednimi sestanki, ki bodo potekali na fakulteti ali pa doma, ter preko različnih socialnih omrežij.
* Prvi lasten projekt smo si razdelili takole:

|                             | Nik   | Sara | Žan  | Žiga 
| :-------------------------- | :---- | :--- | :--- | :--- 
| Povzetek projekta           |       | 100% |      |
| Motivacija                  |       | 100% |      |                            | ...                        |
| Pričakovani rezultat        |       |      |      | 100%
| Načrt posameznih aktivnosti |       |      | 100% |
| Seznam izdelkov             |       | 100% |      |
| Ganttov diagram             |       |      |      | 100%
| Graf Pert                   |       | 100% |      |
| Načrt obvladovanja tveganj  |       |      |      | 100%
| Ekipa                       | 100%  |      |      |
| Finančni načrt              | 100%  |      |      |


## 6. Predstavitev skupine

* Nik Bratuša, 21, študent 3. letnika Univerze v Ljubljani na fakulteti za računalništvo
in informatiko. Nik je vodja te ekipe. Zadolžen pa bo predvsem za delo na back-endu z Žanom Pečovnikom in za
upravljenje projekta. Njegove kompetence
vključujejo znanje JavaScript, Java, C, Python, JQuery, NodeJS, SQL, Matlab, HTML in CSS.

* Sara Koljić, 21, študentka 2. letnika Univerze v Ljubljani na fakulteti za računalništvo
in informatiko. Njena naloga bo vsebovala delo na front-endu skupaj z Žigo Kleinetom, 
predvsem na področju izgleda same aplikacije zaradi njene kreativnosti. Njena znanja vsebujejo
JavaScript, Java, SQL, Matlab, HTML, CSS in C. 

* Žan Pečovnik, 21, študent 3. letnika Univerze v Ljubljani na fakulteti za računalništvo
in informatiko. Njegova znanja so močna na področju back-enda predvsem v povezavi
z implementacijo API-jev, zato bo tudi predstavljav glavnega back-end razvijalca.
Seveda je vešč v naslednjih tehnologijah : JavaScript, Java, Matlab, C, Python, JQuery, NodeJS, SQL, PHP, HTML in CSS.

* Žiga Kleine, 21, študent 2. letnika Univerze v Ljubljani na fakulteti za računalništvo in 
informatiko. Žiga je najbolj vešč v HTML-ju in CSS-ju zato bo predstavljal vodjo razvoja
front-enda. Prav tako vsebujejo njegova znanja še JavaScript, Javo, C, SQL, Python in Matlab.


## 7. Finančni načrt - COCOMO II ocena

Urna postavka pri projektu je postavljena na 10€ na uro. Ker je 1 ČM enak 60 ur, je cena enega ČM-ja enaka 600€.
Posredni stroški bodo v vrednosti 20% urne postavke.

| Aktivnost      | Naziv aktivnosti                       | Obseg aktivnosti (ČM) | Neposredni štroški (EUR)    | Posredni stroški (EUR)     | Skupaj (EUR)
| :------------- | :------------                          | :-----------          | :-------------------------- | :------------------------- | :------------
| A1             | Zajem zahtev                           | 0,25             |         150                  |     30           |  180
| A2             | Obvladovanje projekta in tveganj       | 0,1               |           60                |       12         | 72
| A3             | Izobraževanje o problemski domeni      | 0,15 |            90 |                           18  | 108
| A4             | Načrtovanje arhitekture                | 0,1 | 60 |                12    |            72        
| A5             | Načrt urnika,redovalnice in vpisa      | 0,05 |  30    |      6 | 36
| A6             | Načrt urnika lpp in študentske prehrane| 0,1   |   60 |       12 | 72
| A7             | Načrtovanje podatkovne baze            | 0,1 | 60 |  12 | 72
| A8             | Načrtovanje testiranja                 | 0,05 |  30       | 6 | 36  
| A9             | Project management                     | 0,05  |   30    |   6 | 36
| A10            | Izgled urnika avtobusov in prehrane    | 0,09   |    54    |     10,8    | 64,8              
| A11            | Izgled urnika,redovalnice in vpisa     | 0,12    |     72 |      14,4     |    86,4        
| A12            | Testiranje front-end                   | 0,05 |   30 | 6 | 36
| A13            | Ravzijanje podatkovne baze             | 0,07 |  42 | 8,4 | 50,4
| A14            | Testiranje baze                        | 0,03 | 18 | 3,6 | 21,6
| A15            | Ravzijanje back-end part 1             | 0,09 | 54 | 10,8 | 64,8
| A16            | Ravzijanje back-end part 2             | 0,17 | 102 | 20,4 | 122,4
| A17            | Testiranje back-end                    | 0,03 | 18 |  3,6 | 21,6
| A18            | Integracija                            | 0,14 | 84 | 16,8 | 100,8
| A19            | Test integracije                       | 0,07 | 42 | 8,4 | 50,4
| A20            | Dokumentacija                          | 0,07 | 42 | 8,4 | 50,4
| A21            | Predstavitev                           | 0,03 | 18 | 3,6 | 21,6

Skupna vsota vseh stroškov je enaka cca. 1375€.

Ocenjevanje po metodi COCOMO II:

Naša aplikacija bo vsebovala:

* EI - Prijava - L - 3
* EI - Registracija - L - 3
* EQ - Prikaz urnika - L - 3
* EQ - Prikaz redovalnice - L - 3
* EI - Vnos in izbris aktivnosti v urniku - L - 3
* EI - Vnos in izbris predmeta v redovalnici - L - 3
* EI - Vnos in izbris ocene v redovalnici - L - 3
* EI - Iskanje linije LPP - L - 3
* EI - Iskanje restavracij - L - 3
* EQ - Prikaz linij lpp - A - 4
* EQ - Prikaz restevracij in jedilnika - A - 4
* ILF - Podatkovna baza - L - 7
* EIF - Dostop do API-jev LPP in Studenske prehrane - A - 7

Če seštejemo uteži dobimo skupaj 49. Ker bomo aplikacijo razvijali večinoma v jeziku JavaScript, moramo
pomnožiti 49 * 47, kar je enako 2303. Torej KSLOC = 2303 = Obseg.


Parameter A = 2,94.


Za izračun parametra B smo uporabili: 
* PREC = 4
* FLEX = 3
* RESL = 4
* TEAM = 2
* RMAT = 5


Torej B = 1,01 * 0,01 * 18 = 1,19.

Za parameter M smo uporabili:
* PERS = 1,2
* PREX = 1,2
* RCPX = 1
* RUSE = 0,7
* PDIF = 1,1
* SCED = 1
* FCIL = 1

M je zmožek vseh, torej je enak 1,109.


Končen izračun napora je torej enak: 2,94 * 2,303^1,19 * 1,109 = 8,79 ČM.


Projekt torej zahteva 8,79 ČM dela.




## Reference

[1]: Navodila https://teaching.lavbic.net/TPO/2018-2019/LP1.html  

[2]: LPP prevozi http://www.lpp.si/  

[3]: Študentska prehrana https://www.studentska-prehrana.si/sl  

[4]: Ganttov diagram https://www.teamgantt.com/

[5]: Pertov diagram https://www.lucidchart.com